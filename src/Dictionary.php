<?php
namespace Framework\Langoose;

class Dictionary
{

    public static function bn2en(string $bangla)
    {
        return str_replace(['১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯', '০'], ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'], $bangla);
    }

    public static function en2bn(string $english)
    {
        return str_replace(['1', '2', '3', '4', '5', '6', '7', '8', '9', '0'], ['১', '২', '৩', '৪', '৫', '৬', '৭', '৮', '৯', '০'], $bangla);
    }
}
